import Vue from 'nativescript-vue'
import Home from './components/Home.vue'
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'

// Import Bootstrap and BootstrapVue CSS files (order is important)
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

// Make BootstrapVue available throughout your project
Vue.use(BootstrapVue)
// Optionally install the BootstrapVue icon components plugin
Vue.use(IconsPlugin)

// axios.defaults.headers.common["Authorization"] = "Basic YWRtaW46MTIzNDU2"
// axios.defaults.baseURL = 'http://84.252.142.58';

declare let __DEV__: boolean;

// Prints Vue logs when --env.production is *NOT* set while building
Vue.config.silent = !__DEV__
Vue.config.silent = false;
Vue.config.productionTip = false;

new Vue({
  render: (h) => h('frame', [h(Home)]),
}).$start()

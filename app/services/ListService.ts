import {IListResponse, ProductModel} from "~/services/models/ProductModel";
import {Settings} from "~/services/Settings";
import {API} from "~/services/api";

export default class ListService {
  private products: ProductModel[] = [
    {"name": "Яйца", "count": 2, "unit": "шт.", "check": false, "category": [], density: 1},
    {"name": "Пармезан", "count": 200, "unit": "гр.", "check": false, "category": [], density: 1},
    {"name": "Картошка", "count": 1, "unit": "кг.", "check": false, "category": [], density: 1},
  ]

  async getProducts(id: string) {
    const api = new API();
    const result: Response = await api.get(`/list/${id}`);
    if (result.status !== 406) {
      const r_json: IListResponse = await result.json();
      this.products = r_json.products;
    }
    else this.products = [];
    return this.products
  }

  getProductByTitle(name: string): ProductModel | undefined {
    return this.products.find(product => product.name === name) || undefined
  }
}

import {ProductModel} from "~/services/models/ProductModel";
import {Settings} from "~/services/Settings";
import {API} from "~/services/api";
const req = new API();

// Сервис поиска
export default class ProductService {
  private products: ProductModel[] = [
    {"name": "Яйца", "count": 2, "unit": "шт.", "check": false, "category": [], density: 1},
    {"name": "Пармезан", "count": 200, "unit": "гр.", "check": false, "category": [], density: 1},
    {"name": "Картошка", "count": 1, "unit": "кг.", "check": false, "category": [], density: 1},
  ]

  getProducts(query: string | undefined): ProductModel[] {
    console.log(`Get products for ${query}`)
    if (query === undefined)
      console.log("Берём случайный набор продуктов для поиска")

    if (query == 's')
      return [
        {"name": "Яйца", "count": 2, "unit": "шт.", "check": false, "category": [], density: 1},
        {"name": "Пармезан", "count": 200, "unit": "гр.", "check": false, "category": [], density: 1},
        {"name": "Cекрет", "count": 1, "unit": "кг.", "check": false, "category": [], density: 1},
      ]
    return this.products
  }

  async addProduct(list_id: string, product: ProductModel) {
    const result: Response = await req.put(`/edit/${list_id}`, product);
  }
  async rmProduct(list_id: string, product: ProductModel) {
    const result: Response = await req.put(`/delete_product/${list_id}`, product);
  }
}

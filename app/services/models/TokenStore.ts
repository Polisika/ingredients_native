export interface IUserProfile {
  id: string;
  user_id: string;
}

export interface MainState {
  token: string;
  isLoggedIn: boolean | null;
  userProfile: IUserProfile | null;
}

const defaultState: MainState = {
  isLoggedIn: null,
  token: "",
  userProfile: null,
};

export const mutations = {
  setToken(state: MainState, payload: string) {
    state.token = payload;
  },
  setLoggedIn(state: MainState, payload: boolean) {
    state.isLoggedIn = payload;
  }
}

export interface ProductModel {
  name: string
  count: number
  unit: string
  check: boolean | undefined
  category: Array<string>
  density: number
}

export interface IListResponse {
  list_id: string
  name: string
  products: Array<ProductModel>
}

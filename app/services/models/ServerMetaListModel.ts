// {
//   "lists": [
//     {
//       "name": "new list",
//       "number_of_products": 2,
//       "number_of_checked_products": 1
//     }
//   ]
// }

export interface MetaList {
  id: string
  name: string
  number_of_products: string
  number_of_checked_products: string
}
export interface ServiceResponse {
  lists: Array<MetaList>
}

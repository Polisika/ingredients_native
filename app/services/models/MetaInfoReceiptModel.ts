export interface MetaInfoReceiptModel {
  id: string
  title: string
  productCount: string
  listId: string
}

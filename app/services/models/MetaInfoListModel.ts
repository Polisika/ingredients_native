export interface MetaInfoListModel {
  id: string
  name: string
  number_of_products: string // "2 recipes"
  number_of_checked_products: string // "4/10"
}

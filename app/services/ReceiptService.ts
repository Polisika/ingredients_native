import {MetaInfoReceiptModel} from "~/services/models/MetaInfoReceiptModel";
import {ProductModel} from "~/services/models/ProductModel";
import {API} from "~/services/api";

export default class ReceiptService {
  private recipes: MetaInfoReceiptModel[] = [
    {id: "123", title: "Лазанья", productCount: "5 продуктов", listId: "123"},
    {id: "123", title: "Пирожки", productCount: "2 продуктов", listId: "123"}
  ]

  async getRecipes() {
    const api = new API();
    const res = await api.get("/info/recipes");
    if (res.status === 406)
      return []
    else
      return await res.json();
  }

  getList(recipe_id: string): ProductModel[] {
    console.log(`Get list of products for ${recipe_id}`)
    return [
      {"name": "Яйца", "count": 2, "unit": "шт.", "check": false, "category": [], density: 1},
      {"name": "Пармезан", "count": 200, "unit": "гр.", "check": false, "category": [], density: 1},
      {"name": "Cекрет", "count": 1, "unit": "кг.", "check": false, "category": [], density: 1},
    ]
  }

  getRecipeByID(id: string): MetaInfoReceiptModel | undefined {
    return this.recipes.find(recipeInfo => recipeInfo.id === id) || undefined
  }
}

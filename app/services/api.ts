import {Settings} from "~/services/Settings";

const settings = new Settings();

export class API {
  get(url: string) {
    return fetch(settings.host + url, {headers: {Authorization: settings.token}}).then((res) => {
      console.log(`GET ${url} ${res.status}`);
      return res;
    })
  }

  post(url: string, json: object) {
    return this._req("POST", settings.host + url, json);
  }

  put(url: string, json: object) {
    return this._req("PUT", settings.host + url, json);
  }

  delete(url: string) {
    return fetch(settings.host + url,
      {
        headers: {Authorization: settings.token},
        method: "DELETE"
      }).then((res) => {
      console.log(`DELETE ${url} ${res.status}`);
      return res;
    })
  }

  private _req(method: string, url: string, json: object) {
    return fetch(url, {
      headers: {
        Authorization: settings.token,
        'Content-Type': 'application/json',
      },
      method: method,
      body: JSON.stringify(json)
    }).then((res) => {
      console.log(`${method} ${url} ${JSON.stringify(json)} ${res.status}`);
      return res;
    })
  }
}

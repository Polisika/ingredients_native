import {MetaInfoListModel} from "~/services/models/MetaInfoListModel";
import {ServiceResponse} from "~/services/models/ServerMetaListModel";
import {Settings} from "~/services/Settings";
import {API} from "~/services/api";

export default class ListsService {
  private lists: ServiceResponse = {lists: []}

  async getLists() {
    // {id: "123", title: "Happy birthday! 10.10.22", recipesNum: "4 recipes", checked: "4/10"},
    // {id: "123", title: "New Year", recipesNum: "1 recipes", checked: "0/18"}
    const api = new API();
    let resp = await api.get(`/info`);

    let r_json = await resp.json();
    if (r_json.detail === "Not authenticated")
      throw new Error("-----------------------------------------------------------YOU NEED AUTHORIZE TO SERVICE Not authorized");
    this.lists = r_json as ServiceResponse;
    return this.lists;
  }

  getListByID(id: string): MetaInfoListModel | undefined {
    // @ts-ignore
    return this.lists.lists.find(listInfo => listInfo.id === id) || undefined
  }
}
